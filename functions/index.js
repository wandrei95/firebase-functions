const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

exports.sendCollaborationRequestNotification = functions.database.ref('/doctors/{docId}/collaborationRequests/{collabReqId}')
  .onWrite((change, context) => {
    const doctorId = context.params.docId;

    const getDeviceTokensPromise = admin.database().ref('/doctors/' + doctorId + '/fcmToken').once('value');

    let tokensSnapshot;
    let tokens;

    return Promise.all([getDeviceTokensPromise]).then(results => {
      tokensSnapshot = results[0];

      console.info('Token ', tokensSnapshot);
      console.info('Tokens ', tokensSnapshot.val());

      // Notification details.
      const payload = {
        notification: {
          title: 'Collaboration request',
          body: `You have a new collaboration request`,
        }
      };

      tokens = Object.keys(tokensSnapshot.val());
      return admin.messaging().sendToDevice(tokensSnapshot.val(), payload);
    }).then((response) => {
      // For each message check if there was an error.
      const tokensToRemove = [];
      response.results.forEach((result, index) => {
        const error = result.error;
        if (error) {
          console.error('Failure sending notification to', tokens[index], error);
          // Cleanup the tokens who are not registered anymore.
          if (error.code === 'messaging/invalid-registration-token' ||
            error.code === 'messaging/registration-token-not-registered') {
            tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
          }
        }
      });
      return Promise.all(tokensToRemove);
    });
  });

exports.addMyDoctor = functions.database.ref('/doctors/{docId}/acceptedPatients/{collabReqId}')
  .onCreate((change, context) => {
    const original = change.val();

    var docId = context.params.docId;
    var patId = context.params.collabReqId;

    const getDoctorDetailsPromise = admin.database().ref('/doctors/' + docId + '/details').once('value');
    const getFcmTokenPromise = admin.database().ref('/doctors/' + docId + '/fcmToken').once('value');

    return Promise.all([getDoctorDetailsPromise, getFcmTokenPromise]).then(results => {
      var doctorDetailsResult = results[0];
      var fcmTokenResult = results[1];

      var db = admin.database();
      var ref = db.ref("patients/" + patId + "/myDoctors/" + docId);

      return ref.set({
        fcmToken: fcmTokenResult.val(),
        firstName: doctorDetailsResult.val().firstName,
        lastName: doctorDetailsResult.val().lastName,
        id: docId
      });
    });
  });

  exports.removeMyDoctor = functions.database.ref('/doctors/{docId}/acceptedPatients/{patientId}')
  .onDelete((change, context) => {
    const original = change.val();

    var docId = context.params.docId;
    var patId = context.params.patientId;

    var db = admin.database();
    var ref = db.ref('/patients/' + patId + '/myDoctors/' + docId);
    
    return ref.remove();
  });

  exports.removeAcceptedPatient = functions.database.ref('/patients/{patientId}/myDoctors/{doctorId}')
  .onDelete((change, context) => {
    const original = change.val();

    var docId = context.params.doctorId;
    var patId = context.params.patientId;

    var db = admin.database();
    var ref = db.ref('/doctors/' + docId + '/acceptedPatients/' + patId);
    
    return ref.remove();
  });

exports.verifyHeartRateValue = functions.database.ref('/measurement/heartRate/{patientId}/values/{id}')
  .onWrite((change, context) => {
    const heartRate = change.after.val();
    const patId = context.params.patientId;
    const heartRateId = context.params.id;

    const getHeartRateAvgForPatientPromise = admin.database().ref('/measurement/heartRate/' + patId + '/avg').once('value');
    const getPatientPromise = admin.database().ref('/patients/' + patId).once('value');

    return Promise.all([getHeartRateAvgForPatientPromise, getPatientPromise]).then(results => {
      var heartRateAvgResult = results[0].val();
      var patientResult = results[1].val();
      var doctorsResult = patientResult.myDoctors;

      const promises = [];
      var db = admin.database();
      var ref;
      var i;
      var len;
      var name;

      if (heartRateAvgResult === null) {
        if (heartRate.value < 60 || heartRate.value > 100) {
          Object.keys(doctorsResult).forEach(function (key) {
            ref = db.ref('/doctors/' + doctorsResult[key].id + '/alarms').push();
            promises.push(ref.set({
              patientId: patId,
              patientFirstName: patientResult.details.firstName,
              patientLastName: patientResult.details.lastName,
              alarmType: 'heartRate',
              value: heartRate.value,
              timestamp: Date.now()
            }));
          });
        } else {
          ref = db.ref('/measurement/heartRate/' + patId)
          promises.push(ref.update({
            avg: heartRate.value,
          }));
        }
      } else {
        if (Math.abs(heartRateAvgResult - heartRate.value) > 10 || heartRate.value < 60 || heartRate.value > 100) {
          Object.keys(doctorsResult).forEach(function (key) {
            ref = db.ref('/doctors/' + doctorsResult[key].id + '/alarms').push();
            promises.push(ref.set({
              patientId: patId,
              patientFirstName: patientResult.details.firstName,
              patientLastName: patientResult.details.lastName,
              alarmType: 'heartRate',
              value: heartRate.value,
              timestamp: Date.now()
            }));

            ref1 = db.ref('/measurement/heartRate/' + patId + '/values/' + heartRateId);
            promises.update(ref1.update({
                state: 'ABNORMALITY'
            }));

          });
        } else {
          ref = db.ref('/measurement/heartRate/' + patId)
          promises.push(ref.update({
            avg: (heartRate.value + heartRateAvgResult) / 2,
          }));
        }
      }
      return Promise.all(promises);
    })
  });

exports.sendAlertNotification = functions.database.ref('/doctors/{docId}/alarms/{alarmId}')
  .onCreate((snapshot, context) => {
    const alarm = snapshot.val();
    const doctorId = context.params.docId;

    const getDeviceTokenPromise = admin.database().ref('/doctors/' + doctorId + '/fcmToken').once('value');

    return Promise.all([getDeviceTokenPromise]).then(results => {
      const tokenSnapshot = results[0];

      // Notification details.
      const payload = {
        notification: {
          title: 'New Alarm',
          body: alarm.patientFirstName + ' ' + alarm.patientLastName + ' has a ' + alarm.alarmType + ' alarm',
        }
};
      
      return admin.messaging().sendToDevice(tokenSnapshot.val(), payload);
    })
  });